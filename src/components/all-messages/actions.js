// constants
import {
    LOAD_MESSAGES,
    ADD_NEW_MESSAGE,
    DELETE_MESSAGE,
    EDIT_MESSAGE
} from "./constants";

export const loadMessages = data => {

    return {
        type: LOAD_MESSAGES,
        data
    }
}

export const addNewMessage = data => {

    return {
        type: ADD_NEW_MESSAGE,
        data
    }
}

export const deleteMessage = id => {

    return {
        type: DELETE_MESSAGE,
        id
    }
}

export const editMessage = (id, text) => {

    return {
        type: EDIT_MESSAGE,
        id,
        text
    }
}