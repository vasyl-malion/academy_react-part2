// core
import React, {useState} from "react";
import {connect} from "react-redux";
import {bindActionCreators} from "redux";

// actions
import {addNewMessage} from "../actions";

// styles
import "./input-my-message.css";

const InputMessage = ({addNewMessage}) => {

    const [text, setText] = useState('');

    const messageSend = () => {
        addNewMessage(text)
        setText('')
    }

    const keyFunc = e => {
        const enterKeyCode = 13;
        if (e.keyCode === enterKeyCode) {
            addNewMessage(text);
            setText('')
        }
    }

    return <div className="inputDiv">
        <input
            value={text}
            placeholder="Type your message"
            onKeyUp={keyFunc}
            onChange={e => setText(e.target.value)}
        />
        <button onClick={messageSend}>Send</button>
    </div>
}

const mapDispatchToProps = dispatch => bindActionCreators({
    addNewMessage: data => addNewMessage(data)
}, dispatch);

export default connect(null, mapDispatchToProps)(InputMessage)