// core
import React from "react";
import {connect} from "react-redux";

// components
import {Message} from "../message/message";
import MyMessage from "../my-message/my-message";

// styles
import "./message-list.css";

const MessageList = ({myMessages, deleteItem, messagesList, updateItem}) => {

    const myArrMessages = myMessages && myMessages.map( message => {
        return <MyMessage
            key = {message.id}
            id={message.id}
            text={message.text}
            createdAt={message.createdAt}
            deleteItem = {deleteItem}
            updateItem = {updateItem}
        />
    });

    const allMessages = messagesList && messagesList.map( message => {
        return <Message
            key = {message.id}
            avatar={message.avatar}
            text={message.text}
            user={message.user}
            createdAt={message.createdAt}
        />
    });

    return <div className="messagesList">
        {allMessages}
        {myArrMessages}
    </div>
}

const mapStateToProps = ({messagesReducer}) => {
    return {
        messagesList: messagesReducer.otherMessages,
        myMessages: messagesReducer.myMessages
    }
}

export default connect(mapStateToProps)(MessageList)
