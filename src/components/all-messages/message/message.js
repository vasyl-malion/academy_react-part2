// core
import React, {useState} from "react";
import {AiFillLike} from "react-icons/ai";

// styles
import "./message.css";

export const Message = ({avatar, user, text, createdAt}) => {

    const [isLiked, setIsLiked] = useState(false);

    const date = createdAt.slice(0,10) + ", " + createdAt.slice(11, 19);

    return  <>
        <div className= "message">
            <img alt="avatar" src = {avatar}/>
            <div className = "name-text">
                <div className="nickname"> {user} </div>
                <div className="text"> {text} </div>
                <div className="createdAt"> {date} </div>
            </div>
        </div>
        <div className="messageLike">
            <AiFillLike
                onClick = {() => setIsLiked(!isLiked)}
                className = {isLiked ? "liked" : "notLiked"}
            />
        </div>
    </>
}