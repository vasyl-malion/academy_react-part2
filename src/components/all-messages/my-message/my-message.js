// core
import React from "react";
import {connect} from "react-redux";
import {bindActionCreators} from "redux";
import {MdDelete} from "react-icons/md";
import {BsPencil} from "react-icons/bs";

// components
import Modal from "../../modal/modal";

// actions
import {deleteMessage} from "../actions";
import {openModal} from "../../modal/actions";

// styles
import "./my-message.css";

const MyMessage = ({id, text, createdAt, deleteMessage, openModal, isEdit}) => {

    const modal = isEdit ? <Modal id = {id}/> : null;

    return <div className="myMessageMain">
        <div key={id} className="myMessage">
            <div>
                <span className="my-message-text"> {text} </span>
                <div className="my-createdAt"> {createdAt} </div>
            </div>
        </div>
        <div className="my-message-actions">
            <div>
                <MdDelete onClick = {() => deleteMessage(id)}/>
            </div>
            <div>
                <BsPencil onClick = {() => openModal(text)} />
            </div>
        </div>
        {modal}
    </div>
}

const mapStateToProps = ({modalReducer}) => {
    return {
        isEdit: modalReducer.isEdit
    }
}

const mapDispatchToProps = dispatch => bindActionCreators({
    deleteMessage: id => deleteMessage(id),
    openModal
}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(MyMessage)