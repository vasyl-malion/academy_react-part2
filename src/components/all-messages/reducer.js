// constants
import {
    ADD_NEW_MESSAGE,
    DELETE_MESSAGE,
    LOAD_MESSAGES,
    EDIT_MESSAGE
} from "./constants";

// services
import {newDate} from "../../services/createData";

// initial state
const initialState = {
    otherMessages: [],
    myMessages: [],
    loading: true
}

export const messagesReducer = (state = initialState, action) => {

    switch (action.type) {
        case LOAD_MESSAGES:
            return {
                ...state,
                otherMessages: action.data,
                loading: false
            };
        case ADD_NEW_MESSAGE:
            return addMessage(action.data, state);
        case DELETE_MESSAGE:
            return deleteItem(action.id, state);
        case EDIT_MESSAGE:
            return editMsg(action.id, action.text, state);
        default:
            return {
                ...state
            };
    }
}

const addMessage = (text, state) => {

    if (!text) {
        return {
            ...state
        }
    }

    const newMessage = {
        id: state.myMessages.length + 1,
        text,
        createdAt: newDate()
    };

    return {
        ...state,
        myMessages: [
            ...state.myMessages,
            newMessage
        ]
    }
}

const deleteItem = (id, state) => {
    let idx = state.myMessages.findIndex( el => el.id === id);

    const newArray = [
        ...state.myMessages.slice(0, idx),
        ...state.myMessages.slice(idx + 1)
    ];

    return {
        ...state,
        myMessages: newArray
    }
};

const editMsg = (id, newText, state) => {

    if (!newText) {
        return {
            ...state
        }
    }

    const idx = state.myMessages.findIndex(msg => msg.id === id);
    const message = state.myMessages[idx]

    const newMsg = {
        ...message,
        text: newText
    }

    const newArray = [
        ...state.myMessages.slice(0, idx),
        newMsg,
        ...state.myMessages.slice(idx + 1)
    ];

    return {
        ...state,
        myMessages: newArray
    }
}
