// core
import React, {useEffect} from "react";
import axios from "axios";
import {connect} from "react-redux";
import {bindActionCreators} from "redux";

// components
import Header from "../header/header";
import MessageList from "../all-messages/message-list/message-list";
import InputMessage from "../all-messages/input-my-message/input-my-message";
import {Loading} from "../loading/loading";

// actions
import {loadMessages} from "../all-messages/actions";

const Chat = ({loadMessages, loading}) => {

    useEffect(() => {
        axios('https://edikdolynskyi.github.io/react_sources/messages.json')
            .then(users => loadMessages(users.data))
    }, [])

    if(loading) {
        return <Loading />
    }

    const messages = loading ? <Loading /> : <><MessageList/><InputMessage /></>

    return <div className="chat">
        <Header/>
        {messages}
    </div>
}

const mapStateToProps = ({messagesReducer}) => {
    return {
        loading: messagesReducer.loading,
    }
}

const mapDispatchToProps = dispatch => bindActionCreators({
    loadMessages: data => loadMessages(data),
}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(Chat)