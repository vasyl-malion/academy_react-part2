// core
import React from "react";
import {connect} from "react-redux";

// styles
import "./header.css";

const Header = ({otherMessages, myMessages}) => {

    let usernamesArr = [];
    let lastMessage = 'xxxx-xx-xx';

    const lastMessageFunc = arr => {
        return arr[arr.length - 1].createdAt.slice(0,10) + ", " + arr[arr.length - 1].createdAt.slice(11, 20);
    }

    function countOfUsers(arr) {
        let result = [];

        for (let str of arr) {
            if (!result.includes(str)) {
                result.push(str);
            }
        }

        return result;
    }

    if (otherMessages) {
        lastMessage = (myMessages.length !== 0) ? lastMessageFunc(myMessages) : lastMessageFunc(otherMessages);
        otherMessages.forEach(user => usernamesArr.push(user.user));
    }

    return <div className="header">
        <div>
            <span> My chat </span>
            <span> {countOfUsers(usernamesArr).length + 1} participants </span>
            <span> {otherMessages.length + myMessages.length} messages </span>
        </div>
        <div>
            <span> last message at {lastMessage} </span>
        </div>
    </div>
}

const mapStateToProps = ({messagesReducer}) => {
    return {
        otherMessages: messagesReducer.otherMessages,
        myMessages: messagesReducer.myMessages
    }
}

export default connect(mapStateToProps)(Header)