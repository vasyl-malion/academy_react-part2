// core
import React, {Fragment} from "react";

// styles
import "./loading.css";

export const Loading = () => {

    return <div className="loading">
        <div className="lds-roller">
            <div></div>
            <div></div>
            <div></div>
            <div></div>
            <div></div>
            <div></div>
            <div></div>
            <div></div>
        </div>
    </div>
}