// core
import React, {useState} from "react";
import {bindActionCreators} from "redux";
import {connect} from "react-redux";

// actions
import {editMessage} from "../all-messages/actions";
import {closeModal} from "./actions";

// styles
import "./modal.css";

const Modal = ({id, text, closeModal, editMessage}) => {

    const [updatedMessage, setUpdatedMessage] = useState(text);

    const saveEdit = () => {
        editMessage(id, updatedMessage);

        if(updatedMessage) {
            closeModal();
        }
    }

    return <div className="modal">
        <span> Edit message </span>
        <textarea
            value={updatedMessage}
            onChange={e => setUpdatedMessage(e.target.value)}
        />
        <div className="modal-btn">
            <span>
                <button onClick={closeModal}> Cancel </button>
            </span>
            <button onClick={saveEdit}> Edit </button>
        </div>
    </div>
}

const mapStateToProps = ({modalReducer}) => {
    return {
        text: modalReducer.initialText
    }
}

const mapDispatchToProps = dispatch => bindActionCreators({
    editMessage: (id, text) => editMessage(id, text),
    closeModal
}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(Modal)