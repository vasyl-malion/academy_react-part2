import {OPEN_MODAL, CLOSE_MODAL} from "./constants";

const initialState = {
    isEdit: false,
    initialText: "",
}

export const modalReducer = (state = initialState, action) => {

    switch (action.type){
        case OPEN_MODAL:
            return {
                ...state,
                isEdit: true,
                initialText: action.text
            }
        case CLOSE_MODAL:
            return {
                ...state,
                isEdit: false
            }
        default:
            return {
                ...state
            }
    }
}