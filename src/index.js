// core
import React from 'react';
import ReactDOM from 'react-dom';
import {store} from "./redux/store";
import {Provider} from "react-redux";

// components
import Chat from "./components/chat/chat";

// styles
import './index.css';

ReactDOM.render(
    <Provider store={store}>
        <React.StrictMode>
            <Chat />
        </React.StrictMode>
    </Provider>,
    document.getElementById('root')
);

