// core
import {combineReducers} from "redux";

// reducers
import {modalReducer} from "../components/modal/reducer";
import {messagesReducer} from "../components/all-messages/reducer";

export const rootReducer = combineReducers({
    modalReducer,
    messagesReducer
})