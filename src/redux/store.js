// core
import {createStore} from "redux";

// reducers
import {rootReducer} from "./rootReducer";

export const store = createStore(rootReducer);